package vlad;


import java.util.*;

public class Main {

    public static void main(String[] args) {

        Set<String> strings2 = new HashSet<>();
        strings2.add("to do smth 1");
        strings2.add("to do smth 2");

        Fish fish = new Fish("Dorry", 1, 10, strings2);

        fish.habits.add("saadfas");
        System.out.println(fish.habits);
        System.out.println(fish);

        Man human1 = new Man("Vlad", "Vlad");

        Map<String, String> string3 = new HashMap<>();
        string3.put(DayOfWeek.MONDAY.name(), "to do smth 1");
        string3.put(DayOfWeek.SUNDAY.name(), "to do smth 2");

        human1.setSchedule(string3);
        System.out.println(human1);

        Human mother = new Woman();
        Human father = new Man();
        Family family = new Family(mother,father);

        Human valera = new Man("valera","valerian");

        family.addChild(valera);
        System.out.println(valera.getFamily());

        family.deleteChild(valera);
        System.out.println(valera.getFamily());

        Dog dog = new Dog("Kuzya", 5, 20, strings2);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(fish);
        family.setPet(pets);
        System.out.println(family);
    }
}


